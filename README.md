# 南苑life+ 整合

#### 介绍
这是对“南苑life+”小程序所有文件进行整合整理的仓库。

- 产品原型：[南苑life+](http://liaoshulin.gitee.io/nanyuan-life-app/#id=bstcid&p=1_1%E5%BC%80%E5%B1%8F%E9%A1%B5&g=1)
- [南苑life+小程序MRD文档](https://gitee.com/fangqiao07/nanyuan-life---integration/blob/master/%E5%8D%97%E8%8B%91life+%20MRD%E6%96%87%E6%A1%A3.md)
- [南苑life+PRD文档](https://gitee.com/fangqiao07/nanyuan-life---integration/blob/master/%E5%8D%97%E8%8B%91life+PRD%E6%96%87%E6%A1%A3.md)  
